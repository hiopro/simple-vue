/*
 * @Overview     : .post-css-rc
 * @Author       : Zi Jun
 * @Email        : zijun2030@163.com
 * @Date         : 2020-12-24 16:48:51
 * @LastEditTime : 2021-01-07 10:41:45
 * @LastEditors  : Zi Jun
 * @FilePath     : \simple-vue\postcss.config.js
 * @Mark         : Do not edit
 */

const path = require('path');

module.exports = ({ file }) => {
  // vant-ui是基于375px，一般项目设计稿是基于750px
  const designWidth = file.dirname.includes(path.join('node_modules', 'vant')) ? 375 : 750;
  return {
    plugins: {
      autoprefixer: {},
      'postcss-px-to-viewport': {
        unitToConvert: 'px',
        viewportWidth: designWidth,
        unitPrecision: 3,
        propList: ['*'],
        viewportUnit: 'vw',
        fontViewportUnit: 'vw',
        selectorBlackList: ['ignore-'],
        minPixelValue: 1,
        mediaQuery: false,
        replace: true,
        exclude: [],
        landscape: false,
        landscapeUnit: 'vw',
        landscapeWidth: 568,
      },
    },
  };
};
