/*
 * @Overview     : 配置项
 * @Author       : Zi Jun
 * @Email        : zijun2030@163.com
 * @Date         : 2020-12-24 15:26:38
 * @LastEditTime : 2021-01-22 12:36:02
 * @LastEditors  : Zi Jun
 * @FilePath     : \simple-vue\public\config.js
 * @Mark         : 独立文件，修改即时生效
 */

window.$config = {
  // 站点标题
  siteTitle: 'SimpleVue',
  // Iconfont Font class
  iconClassUrl: '//at.alicdn.com/t/font_2292504_qwgi70l7gkr.css',
  // Iconfont Symbol
  iconSymbolUrl: '//at.alicdn.com/t/font_2292513_xqesvi79cem.js',
  // 是否开启VConsole
  showVConsole: false,
  // 是否全局开启导航栏
  fixedNavBar: true,
  // 是否全局底部切换栏
  fixedTabBar: true,
  // 语言: zh-CN(默认)/zh-HK/en-US
  locales: {
    show: true,
    lang: 'zh-CN',
    availableLocales: ['zh-CN', 'zh-HK', 'en-US'],
  },
  axios: {
    // Token键名: Authorization/..bundleRenderer.renderToStream
    tokenKey: '',
    // 请求超时时间
    timeout: 10000,
  },
  // 数据默认存放位置 cookie/sessionStorage/localStorage
  storage: 'localStorage',
  // 在微信/小程序中调用微信sdk
  weixin: {
    false: true,
    // ! 微信公众号APPID,为了安全起见，需要放置env中
    // wxAppId: process.env.wxAppId,
  },
  // 基本的api地址，可按需添加，开发环境需在.evn.development中配置
  baseApiUrl: 'https://api.apiopen.top/',
};
