/*
 * @Overview     : babel.config
 * @Author       : Zi Jun
 * @Email        : zijun2030@163.com
 * @Date         : 2020-12-23 16:51:43
 * @LastEditTime : 2021-01-05 18:03:18
 * @LastEditors  : Zi Jun
 * @FilePath     : \simple-vue\babel.config.js
 * @Mark         : 已采用cdn方式，故无需配置此按需引入
 */

// module.exports = {
//   presets: ['@vue/cli-plugin-babel/preset'],
//   plugins: [
//     [
//       'import',
//       {
//         libraryName: 'vant',
//         libraryDirectory: 'es',
//         style: true,
//       },
//       'vant',
//     ],
//   ],
// };
