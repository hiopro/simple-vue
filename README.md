<div align=center><img src="https://www.hualigs.cn/image/600a316f1aab3.jpg" width="100" height="100"></div>

<div align=center><h1>SimpleVue</h1></div>

# :pushpin: 简介

**:disappointed: 还在为公司临时启动新项目从而重新搭建一套前端环境而烦恼？**  
**:disappointed: 还在为项目文件优化及项目代码规范不知如何处理而一一百度？**  
**:disappointed: 是否有想过不想花时间处理功能性技术只想专注公司业务模块？**

SimpleVue 就是为了解决这些问题！省时间、少踩坑、高效率！
SimpleVue 是一款基于`Vue3.x`+`Vant3.x`+`TypeScript` 的移动端且开箱即用的前端集成式、优化式框架。

# :pushpin: 简单预览

![2021012214013888.png](https://www.hualigs.cn/image/600a6b9396b07.jpg)
![20210122121455.png](https://www.hualigs.cn/image/600a51777653b.jpg)
![20210122121828.png](https://www.hualigs.cn/image/600a52392461d.jpg)

# :pushpin: 优势特色

**:star: 互联网上已有诸多类似的开源式框架，那么 SimpleVue 对比有何优势？**

一张图表示：
![simple_vue.jpg](https://www.hualigs.cn/image/600a483d609dc.jpg)

| 序号 |                      要点                       |
| :--: | :---------------------------------------------: |
|  1   | 技术栈：Vue3.x、Vue-Router4.x、Vuex4.x、Vant3.x |
|  2   |   移动端/全面屏适配：px-to-viewport/px-to-rem   |
|  3   |                多语言国际化支持                 |
|  4   |             全局样式/常用样式统一化             |
|  5   |                 鉴权：登录校验                  |
|  6   |           Axios 精细化封装，Hooks 化            |
|  7   |      项目结构优化：Composition/工程化 思想      |
|  8   |                    路由配置                     |
|  9   |         环境变量配置：独立化 + env 配置         |
|  10  |                Icon/Svg 全系支持                |
|  11  |         微信公众号/Hybrid 开发埋点封装          |
|  12  |                 开发便捷性优化                  |
|  13  |                生产构建性能优化                 |
|  14  |              规范化代码/提交/注释               |
|  15  |                常见项目踩坑规避                 |

> :pencil2: 全部详细过程以图文教程形式《Vue3 从零开始集成》系列记录！

# :pushpin: 项目运行

```shell
# 克隆项目/项目分支
# git clone -b viewport https://gitee.com/zijun2030/simple-vue.git
git clone https://gitee.com/zijun2030/simple-vue.git
# 进入项目目录 simple-vue
cd simple-vue
# 安装依赖
yarn install
# 本地开发启动项目
yarn run serve
```

# :pushpin: 版本日志

> **1.0.0**
>
> 1. `A` 新增主要集成和优化，具体可见预览图

# :pushpin: 注意事项

- 项目基于 `postcss-px-to-viewport`，默认 UI 设计稿宽度是 `750px` ，如果需要调整，可在 `postcss.config.js` 中修改：
  ```javascript
  // vant-ui是基于375px，一般项目设计稿是基于750px，可根据实际而定
  const designWidth = file.dirname.includes(path.join('node_modules', 'vant')) ? 375 : 750;
  ```
- 由于要兼容路由参数的多语言支持，所以在路由中及多语言设置中：
  - `route`多语言设置路由名称(`meta.title`)与路由的 `name` 保持一致
  - `route`多语言设置路由名称全部为小写
  ```javascript
  // route.js
  path: 'detail',
  name: 'ListDetail',
  meta: {
    title: LANG.t('route.listdetail.title'),
  },
  ```
  ```json
  // zh-CN.json
  "route": {
    "listdetail": {
      "title": "列表详情"
    }
  },
  ```
- 配置文件说明
  ```javascript
  // 站点标题
  siteTitle: 'SimpleVue',
  // Iconfont Font class
  iconClassUrl: '//at.alicdn.com/t/font_2292504_qwgi70l7gkr.css',
  // Iconfont Symbol
  iconSymbolUrl: '//at.alicdn.com/t/font_2292513_xqesvi79cem.js',
  // 是否开启VConsole
  showVConsole: false,
  // 是否全局开启导航栏
  fixedNavBar: true,
  // 是否全局底部切换栏
  fixedTabBar: true,
  // 语言: zh-CN(默认)/zh-HK/en-US
  locales: {
    show: true,
    lang: 'zh-CN',
    availableLocales: ['zh-CN', 'zh-HK', 'en-US'],
  },
  axios: {
    // Token键名: Authorization/..bundleRenderer.renderToStream
    tokenKey: '',
    // 请求超时时间
    timeout: 10000,
  },
  // 数据默认存放位置 cookie/sessionStorage/localStorage
  storage: 'localStorage',
  // 在微信/小程序中调用微信sdk
  weixin: {
    false: true,
    // ! 微信公众号APPID,为了安全起见，需要放置env中
    // wxAppId: process.env.wxAppId,
  },
  // 基本的api地址，可按需添加，开发环境需在.evn.development中配置
  baseApiUrl: 'https://api.apiopen.top/',
  ```
