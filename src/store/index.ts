/*
 * @Overview     : Store Index
 * @Author       : Zi Jun
 * @Email        : zijun2030@163.com
 * @Date         : 2020-12-23 16:51:43
 * @LastEditTime : 2021-01-11 16:58:26
 * @LastEditors  : Zi Jun
 * @FilePath     : \simple-vue\src\store\index.ts
 * @Mark         : Do not edit
 */

// import { InjectionKey } from 'vue';
import { createStore } from 'vuex';
import { IS_PROD } from '@/utils/validate';

const files = require.context('./modules', false, /\.[t|j]s$/);
const modules = {};

files.keys().forEach((key: string) => {
  (modules as any)[key.replace(/(\.\/|\.[t|j]s)/g, '')] = files(key).default;
});

export default createStore({
  // state: {},
  // mutations: {},
  // actions: {},
  // modules: {},
  modules,
  strict: !IS_PROD,
});
