/*
 * @Overview     : User store
 * @Author       : Zi Jun
 * @Email        : zijun2030@163.com
 * @Date         : 2021-01-11 11:27:37
 * @LastEditTime : 2021-01-11 18:49:50
 * @LastEditors  : Zi Jun
 * @FilePath     : \simple-vue\src\store\modules\user.ts
 * @Mark         : Do not edit
 */

import { IUserState } from '@/utils/interfaces';
import { getStorage, setStorage, deleteStorage } from '@/utils/methods/storage';
const [SET_TOKEN, LOGOUT, SET_USER_INFO] = ['SET_TOKEN', 'LOGOUT', 'SET_USER_INFO'];

const state: IUserState = {
  userInfo: undefined,
  tokenKey: undefined,
};

export default {
  namespaced: true,

  state,

  mutations: {
    [SET_TOKEN](state: IUserState, tokenValue: string) {
      state.tokenKey = tokenValue;
      setStorage('tokenKey', tokenValue);
    },
    [SET_USER_INFO](state: IUserState, info = {}) {
      state.userInfo = info;
      setStorage('userInfo', JSON.stringify(info));
    },
    [LOGOUT](state: IUserState) {
      state.userInfo = undefined;
      state.tokenKey = undefined;
      deleteStorage('tokenKey');
      deleteStorage('userInfo');
    },
  },

  actions: {
    async loginSave(context: any, payLoad: IUserState) {
      context.commit('SET_TOKEN', payLoad.tokenKey);
      context.commit('SET_USER_INFO', payLoad.userInfo);
    },
    async logoutSave(context: any) {
      context.commit('LOGOUT');
    },
  },

  getters: {
    userInfo(state: IUserState) {
      return state.userInfo || JSON.parse(getStorage('userInfo') || '');
    },
    token(state: IUserState) {
      return state.tokenKey || getStorage('tokenKey') || '';
    },
  },
};
