/*
 * @Overview     : App
 * @Author       : Zi Jun
 * @Email        : zijun2030@163.com
 * @Date         : 2021-01-11 18:59:13
 * @LastEditTime : 2021-01-14 11:19:49
 * @LastEditors  : Zi Jun
 * @FilePath     : \simple-vue\src\store\modules\app.ts
 * @Mark         : Do not edit
 */

import { IAppState } from '@/utils/interfaces';
import { getStorage, setStorage } from '@/utils/methods/storage';
const [SET_LANG, SET_FIXED_NAV_BAR, SET_FIXED_TAB_BAR, SET_OPEN_VCONSOLE] = [
  'SET_LANG',
  'SET_FIXED_NAV_BAR',
  'SET_FIXED_TAB_BAR',
  'SET_OPEN_VCONSOLE',
];
const {
  fixedNavBar,
  fixedTabBar,
  showVConsole,
  locales: { lang },
} = window.$config;

const state: IAppState = {
  lang: '',
  fixedNavBar: undefined,
  fixedTabBar: undefined,
  showVConsole: undefined,
};

export default {
  namespaced: true,

  state,

  mutations: {
    [SET_LANG](state: IAppState, lang: string) {
      state.lang = lang;
      setStorage('lang', lang);
    },
    [SET_FIXED_NAV_BAR](state: IAppState, value: boolean) {
      state.fixedNavBar = value;
      setStorage('fixedNavBar', value);
    },
    [SET_FIXED_TAB_BAR](state: IAppState, value: boolean) {
      state.fixedTabBar = value;
      setStorage('fixedTabBar', value);
    },
    [SET_OPEN_VCONSOLE](state: IAppState, value: boolean) {
      state.showVConsole = value;
      setStorage('showVConsole', value);
    },
  },

  actions: {
    // async langSave(context: any, payLoad: IAppState) {
    //   context.commit('SET_LANG', payLoad.lang);
    // },
  },

  getters: {
    lang(state: IAppState) {
      return state.lang || getStorage('lang') || lang;
    },
    fixedNavBar(state: IAppState) {
      if (typeof state.fixedNavBar === 'boolean') {
        return state.fixedNavBar;
      } else {
        if (['true', 'false'].includes(getStorage('fixedNavBar') as string)) {
          return getStorage('fixedNavBar') === 'true';
        } else {
          return fixedNavBar;
        }
      }
    },
    fixedTabBar(state: IAppState) {
      if (typeof state.fixedTabBar === 'boolean') {
        return state.fixedTabBar;
      } else {
        if (['true', 'false'].includes(getStorage('fixedTabBar') as string)) {
          return getStorage('fixedTabBar') === 'true';
        } else {
          return fixedTabBar;
        }
      }
    },
    showVConsole(state: IAppState) {
      if (typeof state.showVConsole === 'boolean') {
        return state.showVConsole;
      } else {
        if (['true', 'false'].includes(getStorage('showVConsole') as string)) {
          return getStorage('showVConsole') === 'true';
        } else {
          return showVConsole;
        }
      }
    },
  },
};
