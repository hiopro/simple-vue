/*
 * @Overview     : Do not edit
 * @Author       : Zi Jun
 * @Email        : zijun2030@163.com
 * @Date         : 2020-12-24 15:17:51
 * @LastEditTime : 2021-01-21 19:07:23
 * @LastEditors  : Zi Jun
 * @FilePath     : \simple-vue\src\router\router-hook.ts
 * @Mark         : Do not edit
 */

import { nextTick } from 'vue';
import i18n from '@/lang';
import store from '@/store';
import $wxShare from '@/utils/weixin/wx-share';
import { RouteLocationNormalized, RouteLocationRaw, Router } from 'vue-router';
import { loadLocalMessageAsync, setI18nLanguage } from '@/utils/methods/i18n';
import { NotBackRoute } from '@/utils/enums';
import { enumToArray, getBrowser } from '@/utils/methods/common';
import { getStorage } from '@/utils/methods/storage';
import { AxiosCanceler } from '@/utils/axios/axios-cancel';
const { availableLocales } = window.$config.locales;
const axiosCanceler = new AxiosCanceler();

/**
 * @description: 鉴权函数
 * @param {RouteLocationRaw} to
 * @return {*}
 */
const canUserAccess = (router: Router, to: RouteLocationRaw) => {
  if ((to as any).meta.requireAuth) {
    if (!store.getters['user/token']) {
      return true;
    } else {
      router.push({ name: 'Login' });
    }
  }
};

export const setupRouterHook = (router: any) => {
  router.beforeEach(async (to: RouteLocationNormalized) => {
    /* 添加标识，解决浏览器缓存问题 */
    const options = {
      name: to.name as string,
      query: {
        v: new Date().getTime(),
        ...to.query,
      },
    };
    if (!to.query.v) {
      enumToArray(NotBackRoute).includes(to.name as string)
        ? router.replace(options)
        : router.push(options);
      return true;
    }

    /* 权限验证 */
    canUserAccess(router, to);

    /* 设置多语言 */
    const paramsLang = to.query.lang;
    const storeLang = store.getters['app/lang'];
    if (availableLocales.includes(paramsLang as string) && paramsLang !== storeLang) {
      await store.commit('app/SET_LANG', paramsLang);
      await loadLocalMessageAsync(i18n, paramsLang as string);
      setI18nLanguage(i18n, paramsLang as string);
      await nextTick();
    }

    /* 页面设置标题,并更新标题语言 */
    let title = to.meta.title;
    const localLang = getStorage('lang');
    if (localLang) {
      const routeName = (to.name as string).toLowerCase();
      title = i18n.global.t(`route.${routeName}.title`);
      to.meta.title = title;
    }
    title && (document.title = title);

    /* 取消上一个路由的所有请求 */
    axiosCanceler.removeAllPending();

    /* 默认每个页面不开启分享,如果需要开启分享，可在页面内设置 */
    if ((getBrowser().isWeChat || getBrowser().isWXProgram) && window.$config.weixin.show) {
      await $wxShare(undefined, { showMenu: false });
    }
  });
};
