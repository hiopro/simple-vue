/*
 * @Overview     : Demo Router
 * @Author       : Zi Jun
 * @Email        : zijun2030@163.com
 * @Date         : 2021-01-05 18:49:17
 * @LastEditTime : 2021-02-03 10:01:34
 * @LastEditors  : Zi Jun
 * @FilePath     : \simple-vue\src\router\modules\home-page.ts
 * @Mark         : Do not edit
 */

import ListChildren from './children/list-children';
import { NotBackRoute } from '@/utils/enums';
import i18n from '@/lang';
const LANG = i18n.global;

export default [
  {
    path: '/home',
    name: NotBackRoute.HOME,
    meta: {
      title: LANG.t('route.home.title'),
      keepAlive: true,
      fixedTabBar: true,
      requireAuth: false,
    },
    component: () => import(/* webpackChunkName: "home-page-router" */ '@/views/home/Home.vue'),
  },
  {
    path: '/list',
    name: NotBackRoute.LIST,
    meta: {
      title: LANG.t('route.list.title'),
      keepAlive: true,
      fixedTabBar: true,
      requireAuth: false,
    },
    component: () => import(/* webpackChunkName: "home-page-router" */ '@/views/home/List.vue'),
    children: ListChildren,
  },
  {
    path: '/mine',
    name: NotBackRoute.MINE,
    meta: {
      title: LANG.t('route.mine.title'),
      keepAlive: true,
      fixedTabBar: true,
      requireAuth: false,
    },
    component: () => import(/* webpackChunkName: "home-page-router" */ '@/views/home/Mine.vue'),
  },
];
