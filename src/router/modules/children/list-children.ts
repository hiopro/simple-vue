/*
 * @Overview     : List Children
 * @Author       : Zi Jun
 * @Email        : zijun2030@163.com
 * @Date         : 2021-01-15 11:43:08
 * @LastEditTime : 2021-02-03 10:02:50
 * @LastEditors  : Zi Jun
 * @FilePath     : \simple-vue\src\router\modules\children\list-children.ts
 * @Mark         : Do not edit
 */

import i18n from '@/lang';
const LANG = i18n.global;

export default [
  {
    path: 'detail',
    name: 'ListDetail',
    meta: {
      title: LANG.t('route.listdetail.title'),
      keepAlive: false,
      fixedNavBar: true,
      fixedTabBar: false,
      requireAuth: false,
    },
    component: () => import(/* webpackChunkName: "list-children-router" */ '@/views/home/List.vue'),
  },
];
