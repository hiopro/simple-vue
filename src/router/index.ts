/*
 * @Overview     : Router Index
 * @Author       : Zi Jun
 * @Email        : zijun2030@163.com
 * @Date         : 2020-12-23 16:51:43
 * @LastEditTime : 2021-02-03 10:00:57
 * @LastEditors  : Zi Jun
 * @FilePath     : \simple-vue\src\router\index.ts
 * @Mark         : Do not edit
 */

import type { App } from 'vue';
import TabbarLayout from '@/layout/navTabbarLayout/index.vue';
import i18n from '@/lang';
import { setupRouterHook } from './router-hook';
import {
  createRouter,
  createWebHistory,
  RouteLocationNormalized,
  RouteLocationNormalizedLoaded,
  RouteRecordRaw,
} from 'vue-router';
const LANG = i18n.global;

/* 异步路由 */
const asyncRoutersTemp: RouteRecordRaw[] = [];
const routerFiles = require.context('./modules', false, /\.[j|t]s$/);
routerFiles.keys().forEach((name) => {
  asyncRoutersTemp.push(...routerFiles(name).default);
});

/* 固定路由 */
const constantRoutes: RouteRecordRaw[] = [
  {
    path: '/login',
    name: 'Login',
    meta: {
      title: LANG.t('route.login.title'),
      keepAlive: false,
      fixedNavBar: false,
      fixedTabBar: false,
      requireAuth: false,
    },
    component: () => import(/* webpackChunkName: "constant-router" */ '@/views/login/index.vue'),
  },
  {
    path: '/404',
    name: '404',
    meta: {
      title: LANG.t('route.404.title'),
      keepAlive: false,
      fixedNavBar: true,
      fixedTabBar: false,
      requireAuth: false,
    },
    component: () => import(/* webpackChunkName: "constant-router" */ '@/views/abnormal/404.vue'),
  },
  {
    path: '/:pathMatch(.*)*',
    meta: {
      title: LANG.t('route.404.title'),
      keepAlive: false,
      fixedNavBar: true,
      fixedTabBar: false,
      requireAuth: false,
    },
    component: () => import(/* webpackChunkName: "constant-router" */ '@/views/abnormal/404.vue'),
  },
];

const layoutRoutes: RouteRecordRaw[] = [
  {
    path: '/',
    redirect: {
      name: 'Home',
    },
    component: TabbarLayout,
    children: [...constantRoutes, ...asyncRoutersTemp],
  },
];

/* 项目路由: 异步路由 + 固定路由 */
const routes: RouteRecordRaw[] = [...layoutRoutes];

/* 创建路由 */
const router = createRouter({
  history: createWebHistory(),
  routes,
  strict: true,
  scrollBehavior(to: RouteLocationNormalized, from: RouteLocationNormalizedLoaded, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    } else if (to.hash) {
      return {
        el: to.hash,
        behavior: 'smooth',
      };
    } else {
      return { top: 0 };
    }
  },
});

/* APP挂载路由 */
export const setupRouter = (app: App<Element>): void => {
  app.use(router);
  setupRouterHook(router);
};

export default router;
