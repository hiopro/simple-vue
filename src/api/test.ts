/*
 * @Overview     : Test Api
 * @Email        : zijun2030@163.com
 * @Date         : 2020-12-24 15:09:08
 * @LastEditTime : 2021-01-20 10:35:00
 * @LastEditors  : Zi Jun
 * @FilePath     : \simple-vue\src\api\test.ts
 * @Mark         : Do not edit
 */

import { packAxios } from '@/utils/axios';
import { RequestEnum } from '@/utils/enums';

enum Api {
  NEWS = '/getWangYiNews',
  JOKE_LIST = '/getJoke',
  JOKE_DETAIL = '/getSingleJoke',
}

export function getNewsList(params = {}) {
  return packAxios.request(
    {
      url: Api.NEWS,
      method: RequestEnum.GET,
      params,
    },
    {
      isHideRequestLoading: true,
    },
  );
}

export function getJokeList(params = {}) {
  return packAxios.request(
    {
      url: Api.JOKE_LIST,
      method: RequestEnum.GET,
      params,
    },
    {
      isHideRequestLoading: true,
    },
  );
}

export function getJokeDetail(data = {}) {
  return packAxios.request({
    url: Api.JOKE_DETAIL,
    method: RequestEnum.GET,
    data,
  });
}
