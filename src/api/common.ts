/*
 * @Overview     : Common API
 * @Author       : Zi Jun
 * @Email        : zijun2030@163.com
 * @Date         : 2021-01-18 11:48:08
 * @LastEditTime : 2021-01-18 12:15:42
 * @LastEditors  : Zi Jun
 * @FilePath     : \simple-vue\src\api\common.ts
 * @Mark         : Do not edit
 */

import { packAxios } from '@/utils/axios';
import { RequestEnum } from '@/utils/enums';

enum Api {
  WEIXIN = '',
}

/**
 * @description: 获取微信配置
 * @param {*} data
 * @return {*}
 */
export const getWXConfig = (data = {}) => {
  return packAxios.request({
    url: Api.WEIXIN,
    method: RequestEnum.GET,
    params: data,
  });
};
