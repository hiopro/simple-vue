/*
 * @Overview     : Main.ts
 * @Author       : Zi Jun
 * @Email        : zijun2030@163.com
 * @Date         : 2020-12-23 16:51:43
 * @LastEditTime : 2021-01-21 14:30:17
 * @LastEditors  : Zi Jun
 * @FilePath     : \simple-vue\src\main.ts
 * @Mark         : Do not edit
 */

import { createApp } from 'vue';
import i18n from '@/lang';
import { setupRouter } from './router';
import App from './App.vue';
import Vant, { Lazyload } from 'vant';
import store from './store';
import VConsole from 'vconsole';
import wxShare from '@/utils/weixin/wx-share';
import '@/styles/index.less';
import 'vant/lib/index.css';
import '@/utils/events/init';
import { setupSvgComponent } from '@/assets/svg';
import { IS_DEV } from '@/utils/validate';
import { getBrowser } from '@/utils/methods/common';

/* 测试环境可开启vConsole调试工具 */
if (IS_DEV && store.getters['app/showVConsole']) {
  new VConsole();
}

const app = createApp(App);
setupRouter(app);
setupSvgComponent(app);
app.use(Lazyload, { lazyComponent: true });

/* 微信环境中注入分享配置 */
if ((getBrowser().isWeChat || getBrowser().isWXProgram) && window.$config.weixin.show) {
  app.provide('$wxShare', wxShare);
}

app.use(i18n).use(store).use(Vant).mount('#app');
