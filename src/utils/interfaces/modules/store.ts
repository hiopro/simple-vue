/*
 * @Overview     : Store
 * @Author       : Zi Jun
 * @Email        : zijun2030@163.com
 * @Date         : 2021-01-11 14:57:26
 * @LastEditTime : 2021-01-11 17:01:27
 * @LastEditors  : Zi Jun
 * @FilePath     : \simple-vue\src\utils\interfaces\modules\store.ts
 * @Mark         : Do not edit
 */

/* user state */
export interface IUserState {
  userInfo?: Record<string, unknown> | undefined;
  tokenKey?: string | undefined;
}
