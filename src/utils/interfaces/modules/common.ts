/*
 * @Overview     : Common
 * @Author       : Zi Jun
 * @Email        : zijun2030@163.com
 * @Date         : 2020-12-30 18:43:12
 * @LastEditTime : 2020-12-30 18:55:25
 * @LastEditors  : Zi Jun
 * @FilePath     : \simple-vue\src\utils\interfaces\modules\common.ts
 * @Mark         : Do not edit
 */

/* 微信授权 参数对象 */
export interface IWxAuth {
  appId: string;
  redirectUrl: string;
  url: string;
}

/* returnLeftTime 返回对象 */
export interface ILeftTime {
  years: string;
  days: string;
  hours: string;
  minutes: string;
  seconds: string;
}
