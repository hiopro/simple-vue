/*
 * @Overview     : App
 * @Author       : Zi Jun
 * @Email        : zijun2030@163.com
 * @Date         : 2021-01-11 19:00:24
 * @LastEditTime : 2021-01-14 18:02:46
 * @LastEditors  : Zi Jun
 * @FilePath     : \simple-vue\src\utils\interfaces\modules\app.ts
 * @Mark         : Do not edit
 */

/* app state */
export interface IAppState {
  lang?: string | undefined;
  fixedNavBar?: boolean;
  fixedTabBar?: boolean;
  showVConsole?: boolean;
}
