/*
 * @Overview     : Router types
 * @Author       : Zi Jun
 * @Email        : zijun2030@163.com
 * @Date         : 2020-12-28 19:24:12
 * @LastEditTime : 2020-12-30 18:45:30
 * @LastEditors  : Zi Jun
 * @FilePath     : \simple-vue\src\utils\interfaces\modules\router.ts
 * @Mark         : Do not edit
 */

/* 路由meta */
export interface IRouterMeta {
  title?: string;
  icon?: string;
  keepAlive?: boolean;
  requireAuth?: boolean;
}
