/*
 * @Overview     : Weixin
 * @Author       : Zi Jun
 * @Email        : zijun2030@163.com
 * @Date         : 2021-01-18 12:11:45
 * @LastEditTime : 2021-01-18 16:45:15
 * @LastEditors  : Zi Jun
 * @FilePath     : \simple-vue\src\utils\interfaces\modules\weixin.ts
 * @Mark         : Do not edit
 */

/* 获取微信配置接口参数 */
export interface IGetWXConfig {
  url: string;
}

/* 微信配置 */
export interface IWXData {
  debug?: boolean;
  appId: string;
  timestamp: string | number;
  nonceStr: string;
  signature: string;
  jsApiList: string[];
}

/* 微信分享配置 */
export interface IWXShareData {
  title: string;
  desc: string;
  link: string;
  imgUrl: string;
  success: () => unknown;
  cancel: () => unknown;
}

/* 微信分享额外信息 */
export interface IWXShareOptions {
  showMenu?: boolean;
  showItems?: boolean;
  hideItems?: boolean;
}
