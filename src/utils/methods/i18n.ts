/*
 * @Overview     : I18n
 * @Author       : Zi Jun
 * @Email        : zijun2030@163.com
 * @Date         : 2020-12-24 15:27:37
 * @LastEditTime : 2021-01-18 09:49:09
 * @LastEditors  : Zi Jun
 * @FilePath     : \simple-vue\src\utils\methods\i18n.ts
 * @Mark         : Do not edit
 */

import { Locale } from 'vant';
import { createI18n, I18nOptions } from 'vue-i18n';
import { nextTick } from 'vue';
import { LangEnum } from '@/utils/enums';
import vantEnUS from 'vant/es/locale/lang/en-US';
import vantZhCN from 'vant/es/locale/lang/zh-CN';
import vantZhHK from 'vant/es/locale/lang/zh-HK';
import axios from 'axios';

/**
 * @description: 创建i18n
 * @param {ICreateI18nOptions} options 创建参数
 * @return {*}
 */
export const setupI18n = (options: I18nOptions) => {
  const i18n = createI18n(options);
  setI18nLanguage(i18n, (options as any).locale);
  return i18n;
};

/**
 * @description: 设置多语言
 * @param {any} i18n i18n对象
 * @param {string} lang 具体语言
 * @return {*}
 */
export const setI18nLanguage = (i18n: any, lang: string) => {
  if (i18n.mode === 'legacy') {
    i18n.global.locale = lang;
  } else {
    (i18n.global.locale as any).value = lang;
  }
  let htmlLang = 'en';
  if (LangEnum.ZH_CN === lang || LangEnum.ZH_HK === lang) {
    htmlLang = 'zh';
  }
  // 设置 axios请求语言 + html语言
  const headersCommon = axios.defaults.headers.common;
  headersCommon['Accept-Language'] = lang;
  (document.querySelector('html') as HTMLElement).setAttribute('lang', htmlLang);
};

/**
 * @description: 加载多语言
 * @param {any} i18n i18n对象
 * @param {string} lang 具体语言
 * @return {*}
 */
export const loadLocalMessageAsync = async (i18n: any, lang: string) => {
  let messages = {};
  const LOCAL_MESSAGES = await import(`@/lang/${lang}.json`);
  const VANT_MESSAGES = getVantLang(lang);
  messages = {
    ...LOCAL_MESSAGES.default,
    ...(VANT_MESSAGES as any).default,
  };
  i18n.global.setLocaleMessage(lang, messages);
  Locale.use(lang, (VANT_MESSAGES as any).default);
  return nextTick();
};

/**
 * @description: 获取语言描述文字
 * @param {string} value 语言值
 * @return {*}
 */
export const getLangText = (value: string): string => {
  let res = '';
  switch (value) {
    case LangEnum.EN_US:
      res = 'English';
      break;
    case LangEnum.ZH_HK:
      res = '繁體中文';
      break;
    case LangEnum.ZH_CN:
      res = '简体中文';
      break;
    default:
      break;
  }
  return res;
};

export const getVantLang = (value: string): string => {
  let res = '';
  switch (value) {
    case LangEnum.EN_US:
      res = vantEnUS;
      break;
    case LangEnum.ZH_HK:
      res = vantZhHK;
      break;
    case LangEnum.ZH_CN:
      res = vantZhCN;
      break;
    default:
      break;
  }
  return res;
};
