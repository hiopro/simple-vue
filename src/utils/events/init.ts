/*
 * @Overview     : Init event
 * @Author       : Zi Jun
 * @Email        : zijun2030@163.com
 * @Date         : 2020-12-30 14:34:48
 * @LastEditTime : 2021-01-07 19:49:53
 * @LastEditors  : Zi Jun
 * @FilePath     : \simple-vue\src\utils\events\init.ts
 * @Mark         : Do not edit
 */
import { getBrowser } from '@/utils/methods/common';

// 禁止双指/双击放大
document.addEventListener('gesturestart', function (e) {
  e.preventDefault();
});
document.addEventListener('touchstart', function (event) {
  if (event.touches.length > 1) {
    event.preventDefault();
  }
});

/* 在微信中，设置body背景颜色和微信一致 */
if (getBrowser().isWeChat) {
  const bodyEl = document.querySelector('body');
  bodyEl && (bodyEl.style.backgroundColor = '#ededed');
}
