/*
 * @Overview     : Enums
 * @Author       : Zi Jun
 * @Email        : zijun2030@163.com
 * @Date         : 2020-12-30 09:45:57
 * @LastEditTime : 2021-01-13 18:08:45
 * @LastEditors  : Zi Jun
 * @FilePath     : \simple-vue\src\utils\enums.ts
 * @Mark         : Do not edit
 */

/* ********************** app setting ************************ */

// 路由切换动画，具体名称可参考animate
export enum PageTransAttrEnum {
  FADE_IN = 'fadeIn',
  ZOOM_IN_RIGHT = 'zoomInRight',
  ZOOM_OUT_LEFT = 'zoomOutLeft',
}

// 存储类型
export enum StorageType {
  COOKIE = 'cookie',
  SESSION_STORAGE = 'sessionStorage',
  LOCAL_STORAGE = 'localStorage',
}

/* ********************** axios ************************ */

// 请求方法
export enum RequestEnum {
  GET = 'GET',
  POST = 'POST',
  PUT = 'PUT',
  DELETE = 'DELETE',
}

// 网络错误类型
export enum NetworkErrorEnum {
  ERROR = 'Network Error',
  TIMEOUT = 'timeout',
}

// 请求Content类型
export enum ContentTypeEnum {
  JSON = 'application/json;charset=UTF-8',
  FORM_UNLENCODED = 'application/x-www-form-urlencoded;charset=UTF-8',
  RAW = 'application/json;charset=UTF-8',
  FORM_DATA = 'multipart/form-data;charset=UTF-8',
}

// 弹窗类型
export enum ErrorMessageModeEnum {
  NONE = 'none',
  MODEL = 'model',
}

/* ********************** lang ************************ */
export enum LangTypeEnum {
  LOCAL = 'local',
  VANT = 'vant',
}

export enum LangEnum {
  EN_US = 'en-US',
  ZH_CN = 'zh-CN',
  ZH_HK = 'zh-HK',
}

/* ********************** router ************************ */
export enum NotBackRoute {
  HOME = 'Home',
  LIST = 'List',
  MINE = 'Mine',
}
