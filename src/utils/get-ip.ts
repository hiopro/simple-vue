/*
 * @Overview     : Do not edit
 * @Author       : Zi Jun
 * @Email        : zijun2030@163.com
 * @Date         : 2020-12-30 15:54:55
 * @LastEditTime : 2020-12-30 16:17:12
 * @LastEditors  : Zi Jun
 * @FilePath     : \simple-vue\src\utils\get-ip.ts
 * @Mark         : Do not edit
 */

/**
 * @description: 获取ip地址
 * @return {string}
 */

const os = require('os');
const iFaces = os.networkInterfaces();
let ip = '';
out: for (const i in iFaces) {
  for (const j in iFaces[i]) {
    const val = iFaces[i][j];
    if (val.family === 'IPv4' && val.address !== '127.0.0.1') {
      ip = val.address;
      break out;
    }
  }
}
module.exports = ip;
