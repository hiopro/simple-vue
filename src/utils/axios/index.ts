/*
 * @Overview     : Do not edit
 * @Author       : Zi Jun
 * @Email        : zijun2030@163.com
 * @Date         : 2020-12-31 14:22:05
 * @LastEditTime : 2021-01-22 12:34:30
 * @LastEditors  : Zi Jun
 * @FilePath     : \simple-vue\src\utils\axios\index.ts
 * @Mark         : Do not edit
 */

import { VAxios } from './axios';
import i18n from '@/lang';
import {
  IAxiosRequestConfig,
  IAxiosResponse,
  ICreateAxiosOptions,
  IRequestOptions,
} from '@/utils/interfaces';
import { AxiosTransform } from './axios-transform';
import { ContentTypeEnum } from '@/utils/enums';
import { deepMerge } from '@/utils/methods/common';
import { Toast } from 'vant';
import { networkError, ERROR_CODE } from './axios-error';
import { ToastOptions } from 'vant/types/toast';
import { pendingMap } from './axios-cancel';
import { IS_DEV } from '@/utils/validate';
import store from '@/store';

const LOADING_OPTIONS: ToastOptions = {
  message: i18n.global.t('axios.loadingText'),
  forbidClick: true,
  duration: 0,
  loadingType: 'spinner',
};

const transform: AxiosTransform = {
  // 请求之前，处理请求参数
  beforeRequest: (config: IAxiosRequestConfig, options: IRequestOptions) => {
    // 加载请求处理
    const { isHideRequestLoading } = options;
    if (!isHideRequestLoading && !pendingMap.size) {
      Toast.loading(LOADING_OPTIONS);
    }

    // 请求token处理,需与后端协商约定
    const token = store.getters['user/token'];
    const tokenName = window.$config.axios.tokenKey;
    if (token && tokenName) {
      config.headers[tokenName] = tokenName === 'Authorization' ? `Bearer ${token}` : token;
    }
    return config;
  },

  responseInterceptors: (response: IAxiosResponse) => {
    !pendingMap.size && Toast.clear();
    // status为200,返回数据处理
    // 以下为针对性数据处理
    const {
      data: { code, msg },
    } = response;
    if (Number(code) === 200) {
      return response.data;
    } else {
      Toast.fail(msg);
      return Promise.reject(msg);
    }
  },

  responseInterceptorsCatch: (error: any) => {
    // 请求所得到的响应的状态码超出了2xx | 请求完全得不到响应
    !pendingMap.size && Toast.clear();
    if (error.response) {
      const { status } = error.response;
      const msg = ERROR_CODE.find((e) => e.code === status).msg || '';
      msg ? Toast.fail(msg) : networkError(error.message);
    } else {
      networkError(error.message);
    }
    return Promise.reject(error);
  },
};

/**
 * @description: 用于封装默认值
 * @param {Partial} opt 优先取值对象，包含axios配置和自定义配置,默认为undefined
 * @return {*}
 */
const createAxios = (opt?: Partial<ICreateAxiosOptions>) => {
  let baseUrl;
  if (IS_DEV) {
    baseUrl = process.env.VUE_APP_API_URL;
  } else {
    baseUrl = window.$config.baseApiUrl || process.env.VUE_APP_API_URL;
  }
  return new VAxios(
    deepMerge(
      {
        timeout: window.$config.axios.timeout,
        baseURL: baseUrl || '',
        headers: { 'Content-Type': ContentTypeEnum.JSON },
        transform,
        requestOptions: {
          isIgnoreCancelToken: false,
          isHideRequestLoading: false,
        },
      },
      opt || {},
    ),
  );
};

export const packAxios = createAxios();
