/*
 * @Overview     : Axios Error
 * @Author       : Zi Jun
 * @Email        : zijun2030@163.com
 * @Date         : 2020-12-31 13:34:17
 * @LastEditTime : 2021-01-04 15:16:52
 * @LastEditors  : Zi Jun
 * @FilePath     : \simple-vue\src\utils\axios\axios-error.ts
 * @Mark         : Do not edit
 */

import { Toast } from 'vant';
import { NetworkErrorEnum } from '@/utils/enums';
import i18n from '@/lang';

//
export const ERROR_CODE: any[] = [
  { code: 400, msg: i18n.global.t('axios.errorCodeMsg.e400') },
  { code: 401, msg: i18n.global.t('axios.errorCodeMsg.e401') },
  { code: 403, msg: i18n.global.t('axios.errorCodeMsg.e403') },
  { code: 404, msg: i18n.global.t('axios.errorCodeMsg.e404') },
  { code: 408, msg: i18n.global.t('axios.errorCodeMsg.e408') },
  { code: 500, msg: i18n.global.t('axios.errorCodeMsg.e500') },
  { code: 501, msg: i18n.global.t('axios.errorCodeMsg.e501') },
  { code: 502, msg: i18n.global.t('axios.errorCodeMsg.e502') },
  { code: 503, msg: i18n.global.t('axios.errorCodeMsg.e503') },
  { code: 504, msg: i18n.global.t('axios.errorCodeMsg.e504') },
  { code: 505, msg: i18n.global.t('axios.errorCodeMsg.e505') },
];

export const networkError = (message: string): void => {
  let msg: string;
  if (message === NetworkErrorEnum.ERROR) {
    msg = i18n.global.t('axios.unknownNetworkError[0]');
  } else if (message.includes(NetworkErrorEnum.TIMEOUT)) {
    msg = i18n.global.t('axios.unknownNetworkError[1]');
  } else {
    msg = i18n.global.t('axios.unknownNetworkError[2]');
  }
  Toast.fail(msg);
};
