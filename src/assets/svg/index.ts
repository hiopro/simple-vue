/*
 * @Overview     : Do not edit
 * @Author       : Zi Jun
 * @Email        : zijun2030@163.com
 * @Date         : 2021-01-19 10:31:07
 * @LastEditTime : 2021-01-20 14:59:57
 * @LastEditors  : Zi Jun
 * @FilePath     : \simple-vue\src\assets\svg\index.ts
 * @Mark         : Do not edit
 */
import type { App } from 'vue';
import SvgIcon from '@/components/svg-icon/index.vue';

const req = require.context('./', false, /\.svg$/);
const requireAll = (requireContext: any) => requireContext.keys().map(requireContext);
requireAll(req);

/* APP注册全局组件 */
export const setupSvgComponent = (app: App<Element>): void => {
  app.component('SvgIcon', SvgIcon);
};
