/*
 * @Overview     : Lang Index
 * @Author       : Zi Jun
 * @Email        : zijun2030@163.com
 * @Date         : 2020-12-24 15:27:37
 * @LastEditTime : 2021-01-16 13:55:10
 * @LastEditors  : Zi Jun
 * @FilePath     : \simple-vue\src\lang\index.ts
 * @Mark         : Do not edit
 */

import { Locale } from 'vant';
import { I18nOptions } from 'vue-i18n';
import store from '@/store';
import { setupI18n, getVantLang } from '@/utils/methods/i18n';

const { lang, availableLocales } = window.$config.locales;
const locale = store.getters['app/lang'];
const vantLang = getVantLang(locale as string);
const messages = {
  [locale]: {
    ...require(`./${locale}.json`),
    ...(vantLang as any),
  },
};
const createOptions: I18nOptions = {
  locale,
  fallbackLocale: lang,
  availableLocales,
  messages,
};

// 设置本地语言（自定义+vant）
const i18n = setupI18n(createOptions);
Locale.use(locale, vantLang);

export default i18n;
