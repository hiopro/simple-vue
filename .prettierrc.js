/*
 * @Overview     : prettier config
 * @Author       : Zi Jun
 * @Email        : zijun2030@163.com
 * @Date         : 2020-12-23 18:07:25
 * @LastEditTime : 2020-12-29 16:00:24
 * @LastEditors  : Zi Jun
 * @FilePath     : \simple-vue\.prettierrc.js
 * @Mark         : Do not edit
 */

module.exports = {
  semi: true,
  trailingComma: 'all',
  singleQuote: true,
  printWidth: 100,
  tabWidth: 2,
  endOfLine: 'auto',
};
